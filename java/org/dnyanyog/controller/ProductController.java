package org.dnyanyog.controller;

import org.dnyanyog.data.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductController {

	@Autowired
	Product product;
	@GetMapping(path="/api/product")
	public Product getProduct() {
		
		
		
		product.productName = "Pen";
		product.price=(double)5;
		product.quantity=100;
		
		product.category.name="Stationary";
		
		product.variations.variationList.add("Red Color");
		product.variations.variationList.add("Blue Color");
		product.variations.variationList.add("Black Color");
		
		
		
		return product;
		
	}
}
